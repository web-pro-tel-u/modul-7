<?php
    $name = "ARIS PUJUD KURNIAWAN";
    $nim = "12345678";
    $email = "arispujud.k@gmail.com";
    $noHP = "085810000883";
    $noWA = "6285810000883"; //Wajib pakai kode negara (kode Indonesia = 62)
    $tglLahir = "1993-03-13";
    $umur = hitungUmur($tglLahir);

    function hitungUmur($birthdayDate){
        $date = new DateTime($birthdayDate);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }

    echo "Nama Saya: ";
    echo $name;
    echo "<br>";
    echo "NIM Saya: ";
    echo $nim;
    echo "<br>";
    echo "Email Saya: ";
    echo " <a href='mailto://".$email."' target='_blank'>".$email."</a>";
    echo "<br>";
    echo "Ho HP Saya: ";
    echo " <a href='tel://".$noHP."' target='_blank'>".$noHP."</a>";
    echo "<br>";
    echo "Ho WA Saya: ";
    echo " <a href='https://wa.me/".$noWA."' target='_blank'>".$noWA."</a>";
    echo "<br>";
    echo "Tgl Lahir Saya: ";
    echo $tglLahir;
    echo "<br>";
    echo "Umur Saya: ";
    echo $umur." Tahun";
    echo "<br>";
    echo "2 Tahun yang lalu saya berumur ".($umur-2)." Tahun<br>";
    echo "Tahun depan saya akan berumur ".($umur+1)." Tahun<br>";
    echo "<a href='perhitungan.php'>Klik untuk belajar perhitungan dengan PHP</a>";


?>
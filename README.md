### Modul 7

# INTRO PHP

PHP: Hypertext Preprocessor adalah bahasa pemrograman yang bersifat Server Side Scripting (Pemrosesannya ada pada sisi Server). Silahkan pelajari menggunakan handbook dibawah ini:

#### Online Handbook

1. [W3Schools](https://www.w3schools.com/php/default.asp "W3Schools")
2. [Youtube: Web Programming UNPAS](https://www.youtube.com/watch?v=l1W2OwV5rgY&list=PLFIM0718LjIUqXfmEIBE3-uzERZPh3vp6&ab_channel=WebProgrammingUNPAS "Web Programming UNPAS - PHP")

### Penulisan Syntax PHP

Ekstensi file PHP adalah **.php**. Silahkan membuat file dengan nama **index.php**
Penulisan syntax PHP diawali dengan notasi **<?php** dan diakhiri dengan notasi **?>**.

```php
<?php

echo "Kuliah Web Programming 45-02";

?>
```

Di dalam file PHP juga dapat ditambahkan syntax HTML, CSS, JavaScript yang pernah kita pelajari sebelumnya.

```php
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Modul 7 - PHP</title>
</head>
<body>
    <h1>
	<?php
		echo "Kuliah Web Programming 45-02";
	?>
	</h1>
</body>
</html>
```

### Penulisan Variable

Penuisan nama variable selalu diawali dengan karakter $. Variabel tidak perlu didefinisikan terlebih dahulu tipe datanya.

```php
<?php
$namaDepan = "Aris Pujud";
$namaBelakang = "Kurniawan";

echo $namaDepan + " " + $namaBelakang;
?>
```

Pada pemrograman PHP juga memiliki Tipe data, Operasi Aritmatika, Percabangan (If Else, Switch), Perulangan/Looping (For, While, Do While), Function & Procedure seperti pada Algoritma Pemrograman yang telah kalian pelajari. Struktur syntax pemrogramannya juga mirip dengan bahasa C/C++ yg telah kalian pelajari.

### Tugas

Silahkan buka & pelajari contoh program PHP yang ada pada repository ini **index.php** dan **perhitungan.php**

1. Buat program menghitung luas lingkaran dengan nama file **lingkaran.php**.
2. Buat program percabangan menggunakan **switch** dengan nama file **percabangan.php**.
3. Buat program perulangan dengan menggunakan **for** dan **do while** dengan nama file **perulangan.php**

#### Pengumpulan Tugas

1. Semua file dimasukkan dalam folder dengan nama **NIM** kalian.
2. Jadikan folder tersebut menjadi file **.zip**
3. Upload file tersebut melalui link berikut: [https://forms.gle/oefdw3rTjWUwoY267](https://forms.gle/oefdw3rTjWUwoY267 "Link Pengumpulan Tugas")
4. Deadline pengumpulan **Selasa, 29 November 2022 pukul 23:59:59**.
